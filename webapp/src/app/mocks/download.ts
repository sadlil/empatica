import {Download} from "../models/download";

export const MockDownloads : Download[] = [
    {
        "app_id": "hello this is id",
        "longitude": "xx",
        "latitude": "yy",
        "download_at": "cc",
    },
    {
        "app_id": "hello this is id",
        "longitude": "xx",
        "latitude": "yy",
        "download_at": "cc",
    },
    {
        "app_id": "hello this is id",
        "longitude": "xx",
        "latitude": "yy",
        "download_at": "cc",
    },
];