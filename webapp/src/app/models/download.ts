export class Download {
    app_id: string;
    longitude: number;
    latitude: number;
    download_at: string;
    country_name: string;
    country_code: string;
}