import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import 'rxjs/add/operator/map'
import { Download } from '../models/download'

@Injectable()
export class DownloadService {
    constructor(private http: Http) {}

    getDownloads() {
        return this.http.get("/api/v1/downloads").map(response => <Download[]> response.json().download)
    }
}