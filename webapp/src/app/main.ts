import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './moudules/app';

platformBrowserDynamic().bootstrapModule(AppModule);