import { Component } from '@angular/core';
import { Download } from "../models/download";
import { DownloadService } from "../services/download";
import {SocketService} from "../services/socket";

@Component({
    selector: 'application',
    templateUrl: 'app/templates/app.html',
    styleUrls: ['app/styles/app.css'],
})
export class AppComponent {
    downloads: Download[];
    insertText = `$ curl -XPOST --data "{\\"random\\": 5}" localhost:15656/api/v1/downloads
    `;

    constructor(
        private downloadService: DownloadService,
        private socket: SocketService
    ){}

    public ngOnInit() {
        this.socket.getEventListener().subscribe(event => {
            if(event.type == "message") {
                let data = event.data.content;
                if (data == "new") {
                    this.downloadService.getDownloads()
                        .subscribe(downloads => this.downloads = downloads)
                }
            }
            if(event.type == "open") {
                // Subscribe to socket event
                this.socket.send("download");
            }
        });
        this.downloadService.getDownloads()
            .subscribe(downloads => this.downloads = downloads)
    }

    public ngOnDestroy() {
        this.socket.close();
    }
}

