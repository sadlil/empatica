import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AppComponent }  from '../components/app';
import { DownloadService } from "../services/download";
import { AgmCoreModule } from '@agm/core';
import { SocketService } from "../services/socket";

@NgModule({
    imports:      [
        BrowserModule,
        HttpModule,
        // This module load angular2-google-map to render google map
        // on view. This will not work in any kind of prod mode if
        // google map API key is not used.
        // API key is needed to set with the following call.
        AgmCoreModule.forRoot({}),
    ],
    declarations: [ AppComponent ],
    bootstrap:    [ AppComponent ],
    providers:     [
        DownloadService,
        SocketService
    ],
    exports: [
    ],
})
export class AppModule { }
