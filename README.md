## empatica-server
empatica test project solution.

### Build
requirements
 - gRPC
 - protoc
 - npm
 - gRPC gateway

**If you Do not have protoc installed, run**
```
./configure.sh
```

#### Build server
1.
```
make install
```
This will build the server binary in $GOPATH/bin

2.
```
make go-build
```
This will build single binary in bin/

3.
```
make docker-build
```
This will build The docker image of the server


### Run
1. **Run `$GOPATH/bin` binary using**
```
empatica-server serve --v=5
```
This will serve the webapp and api. `--v=5` stands for log verbosity.

2. **Run `./bin` binary using**
```
./bin/empatica-server serve --v=5
```

3. **Run Docker Image**
```
docker run -d -p 15656:15656 -p 15655:15655 -v /tmp/empatica:/tmp/empatica sadlil/empatica:v0.1
```


### Output
- Web interface, served at [http://localhost:15656/](http://localhost:15656/)
- Debug pprof of the server, served at [http://localhost:15655/debug/pprof/](http://localhost:15655/debug/pprof/)
- Debug charts of server, served at [http://localhost:15655/debug/charts/](http://localhost:15655/debug/charts/)
- Prometheus metrics endpoint, served at [http://localhost:15655/metrics](http://localhost:15655/metrics)

**Insert New Download data to Database**
```
curl -XPOST --data "{\download\": [{\"app_id\": \"id\", \"longitude\": 123.234, \"latitude\": 12.234}]}" localhost:15656/api/v1/downloads
```

**Insert some Randomly generated data for testing**
```
curl -XPOST --data "{\"random\": 5}" localhost:15656/api/v1/downloads // You can insert max 5 data at once.
```

Data will be stored in boltdb, at `/tmp/empatica/db/empatica.db`
Inserted data will automatically reflect in the loaded map realtime via `WebSocket`.

