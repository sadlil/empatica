package version

import (
	"fmt"
	"runtime"
)

var (
	versionMajor string = "0"
	versionMinor string = "1"

	version string = "v0.1"
)

type Info struct {
	Major      string
	Minor      string
	GitVersion string
	GoVersion  string
	Compiler   string
	Platform   string
}

func Get() Info {
	// These variables typically come from -ldflags settings and in
	// their absence fallback to the settings in pkg/version/base.go
	return Info{
		Major:      versionMajor,
		Minor:      versionMinor,
		GitVersion: version,
		GoVersion:  runtime.Version(),
		Compiler:   runtime.Compiler,
		Platform:   fmt.Sprintf("%s/%s", runtime.GOOS, runtime.GOARCH),
	}
}
