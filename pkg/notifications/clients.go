package notifications

import (
	"github.com/golang/glog"
	"github.com/gorilla/websocket"
	"github.com/satori/go.uuid"
)

type Client struct {
	id     string
	socket *websocket.Conn
	send   chan []byte
}

func NewClient(conn *websocket.Conn) *Client {
	return &Client{
		id:     uuid.NewV4().String(),
		socket: conn,
		send:   make(chan []byte),
	}
}

func (c *Client) Run() {
	manager.register <- c
	go c.read()
	go c.write()
}

func (c *Client) read() {
	defer func() {
		manager.unregister <- c
		c.socket.Close()
	}()

	for {
		_, message, err := c.socket.ReadMessage()
		if err != nil {
			manager.unregister <- c
			c.socket.Close()
			break
		}
		glog.V(4).Infoln("Subscribing to group", string(message))
		if _, ok := manager.subscription[string(message)]; !ok {
			manager.subscription[string(message)] = make([]*Client, 0)
		}
		manager.subscription[string(message)] = append(manager.subscription[string(message)], c)
	}
}

func (c *Client) write() {
	defer func() {
		c.socket.Close()
	}()

	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			c.socket.WriteMessage(websocket.TextMessage, message)
		}
	}
}
