package notifications

import "github.com/golang/glog"

type ClientManager struct {
	clients      map[*Client]bool
	broadcast    chan []byte
	register     chan *Client
	unregister   chan *Client
	subscription map[string][]*Client
}

type Message struct {
	Sender    string `json:"sender,omitempty"`
	Recipient string `json:"recipient,omitempty"`
	Content   string `json:"content,omitempty"`
}

var manager = &ClientManager{
	broadcast:    make(chan []byte),
	register:     make(chan *Client),
	unregister:   make(chan *Client),
	clients:      make(map[*Client]bool),
	subscription: make(map[string][]*Client),
}

func Manager() *ClientManager {
	return manager
}

func (manager *ClientManager) Start() {
	for {
		select {
		case conn := <-manager.register:
			manager.clients[conn] = true
		case conn := <-manager.unregister:
			if _, ok := manager.clients[conn]; ok {
				glog.Infoln("Closing connetion for", conn.id)
				close(conn.send)
				delete(manager.clients, conn)
			}
		case message := <-manager.broadcast:
			for conn := range manager.clients {
				select {
				case conn.send <- message:
				default:
					close(conn.send)
					delete(manager.clients, conn)
				}
			}
		}
	}
}

func (manager *ClientManager) Notify(group, message string) {
	subscribers, ok := manager.subscription[group]
	if ok {
		for _, conn := range subscribers {
			conn.send <- []byte(message)
		}
	}
}

func (manager *ClientManager) BroadCast(message string) {
	for conn := range manager.clients {
		conn.send <- []byte(message)
	}
}
