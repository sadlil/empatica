package servermachinary

import (
	"net"
	"net/http"
	_ "net/http/pprof"
	"strings"
	"time"

	"github.com/golang/glog"
	"github.com/gorilla/websocket"
	goprom "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	_ "github.com/mkevac/debugcharts"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/soheilhy/cmux"
	downloadapis "gitlab.com/sadlil/empatica/apis/download"
	"gitlab.com/sadlil/empatica/assets"
	"gitlab.com/sadlil/empatica/pkg/apiserver/download"
	"gitlab.com/sadlil/empatica/pkg/notifications"
	"gitlab.com/sadlil/empatica/pkg/storage"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type apiServer struct {
	c *Config
}

func (c *Config) Serve() error {
	server := &apiServer{c: c}
	err := storage.Init(c.Storage, c.StorageInfo)
	if err != nil {
		return err
	}
	defer storage.Close()
	closeChannel := make(chan error)
	go server.RunMonitoringServer(closeChannel)
	go server.Serve(closeChannel)
	go notifications.Manager().Start()

	select {
	case ch := <-closeChannel:
		close(closeChannel)
		return ch
	}
	return nil
}

func (server *apiServer) RunMonitoringServer(channel chan error) {
	if server.c.EnableMetric {
		// Just enabled the metric collection endpoint, There are plenty of scopes
		// Register metrics to prometheus
		glog.V(4).Infoln("Registering Metrics for http server")
		http.Handle("/metrics", promhttp.Handler())
	}

	glog.V(2).Infoln("Serving Monitoring server at ", server.c.MonitoringServerAddress)
	if err := http.ListenAndServe(server.c.MonitoringServerAddress, nil); err != nil {
		glog.Errorln("Failed to start monitoring server, reason", err)
		channel <- err
		return
	}
}

func (server *apiServer) Serve(channel chan error) {
	rootListener, err := net.Listen("tcp", server.c.ServerAddress)
	if err != nil {
		glog.Errorln("Failed to start listner, reason", err)
		channel <- err
		return
	}

	m := cmux.New(rootListener)
	grpcl := m.Match(cmux.HTTP2HeaderField("content-type", "application/grpc"))
	httpl := m.Match(cmux.Any())

	go server.serveGRPC(grpcl, channel)
	time.Sleep(time.Second * 5)
	go server.serveHTTP(httpl, channel)

	if err := m.Serve(); err != nil {
		glog.Errorln("Failed to start cmux, reason", err)
		channel <- err
		return
	}
}

func (server *apiServer) serveGRPC(lis net.Listener, channel chan error) {
	opts := make([]grpc.ServerOption, 0)
	if server.c.EnableMetric {
		opts = append(
			opts,
			grpc.UnaryInterceptor(
				goprom.UnaryServerInterceptor,
			),
		)
	}

	gRPCServer := grpc.NewServer(opts...)
	downloadapis.RegisterDownloadServiceServer(
		gRPCServer,
		download.NewDownloadService(false),
	)

	if server.c.EnableMetric {
		goprom.Register(gRPCServer)
	}

	glog.V(2).Infoln("Starting gRPC server at", server.c.ServerAddress)
	if err := gRPCServer.Serve(lis); err != nil {
		glog.Errorln("Failed to start GRPC server", err)
		channel <- err
		return
	}
}

func (s *apiServer) serveHTTP(lis net.Listener, channel chan error) {
	mux := http.NewServeMux()

	gwmux, err := s.newGatewayMux(lis)
	if err != nil {
		channel <- err
		return
	}

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.URL.Path, "/api/") {
			gwmux.ServeHTTP(w, r)
		} else if strings.HasPrefix(r.URL.Path, "/ws") {
			s.serveWebSocket(w, r)
		} else {
			http.FileServer(assets.LoadWebAPP()).ServeHTTP(w, r)
		}
	})

	srv := &http.Server{
		Addr:         lis.Addr().String(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      mux,
	}
	glog.V(2).Infoln("Starting HTTP server at", s.c.ServerAddress)
	if err := srv.Serve(lis); err != nil {
		glog.Errorln("Failed to start HTTP server", err)
		channel <- err
		return
	}
}

func (a *apiServer) serveWebSocket(res http.ResponseWriter, req *http.Request) {
	conn, error := (&websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}).Upgrade(res, req, nil)
	if error != nil {
		http.NotFound(res, req)
		return
	}
	c := notifications.NewClient(conn)
	c.Run()
}

func (s *apiServer) newGatewayMux(lis net.Listener) (http.Handler, error) {
	ctx := context.Background()
	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}

	addr := lis.Addr().String()
	addr = "127.0.0.1" + addr[strings.LastIndex(addr, ":"):]
	glog.V(4).Infoln("Registering gateway at", addr)
	err := downloadapis.RegisterDownloadServiceHandlerFromEndpoint(ctx, mux, addr, opts)
	if err != nil {
		return nil, err
	}
	return mux, nil
}
