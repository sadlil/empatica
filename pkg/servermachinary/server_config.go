package servermachinary

import (
	"github.com/spf13/pflag"
	"gitlab.com/sadlil/empatica/pkg/utils/logging"
)

func init() {
	logging.Init()
}

type Config struct {
	ServerAddress           string
	EnableMetric            bool
	MonitoringServerAddress string
	Storage                 string
	StorageInfo             string
}

func ServerConfigs() *Config {
	// Set Default Configs
	return &Config{
		ServerAddress:           ":15656",
		EnableMetric:            true,
		MonitoringServerAddress: ":15655",
		Storage:                 "boltdb",
		StorageInfo:             "/tmp/empatica/db/empatica.db",
	}
}

func (c *Config) OverrideFromFlags(flags *pflag.FlagSet) *Config {
	flags.StringVarP(&c.ServerAddress, "server.address", "a", c.ServerAddress, "host:port used to serve web")
	flags.BoolVarP(&c.EnableMetric, "metric", "m", c.EnableMetric, "Enable Promethus Metric Collection")
	flags.StringVarP(&c.MonitoringServerAddress, "monitoring.address", "", c.MonitoringServerAddress, "host:port used to serve pprof and metric")
	flags.StringVarP(&c.Storage, "storage", "", c.Storage, "storage provider")
	flags.StringVarP(&c.StorageInfo, "storage.info", "", c.StorageInfo, "database connection informations")
	return c
}
