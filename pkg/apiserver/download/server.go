package download

import (
	"math"

	"gitlab.com/sadlil/empatica/apis/download"
	"gitlab.com/sadlil/empatica/pkg/storage"
	"golang.org/x/net/context"
)

type downloadService struct{}

func NewDownloadService(mock bool) download.DownloadServiceServer {
	return &downloadService{}
}

func (*downloadService) List(ctx context.Context, req *download.DownloadListRequest) (*download.DownloadListResponse, error) {
	dw, err := storage.NewSession().GetDownloads()
	if err != nil {
		return &download.DownloadListResponse{
			StatusCode: 500,
			Message:    "Failed. Reason: " + err.Error(),
		}, nil
	}
	return &download.DownloadListResponse{
		StatusCode: 200,
		Download:   dw,
	}, nil
}

func (*downloadService) Create(ctx context.Context, req *download.DownloadCreateRequest) (*download.DownloadCreateResponse, error) {
	dataToInsert := make([]*download.Download, 0)
	if req.Random != 0 {
		for i := int(math.Min(float64(req.Random), 5)); i > 0; i-- {
			dataToInsert = append(dataToInsert, randomDownload())
		}
	} else {
		dataToInsert = req.Download
	}
	if err := storage.NewSession().InsertDownloads(dataToInsert...); err != nil {
		return &download.DownloadCreateResponse{
			StatusCode: 500,
			Message:    "Failed. Reason: " + err.Error(),
		}, nil
	}
	return &download.DownloadCreateResponse{
		StatusCode: 200,
		Message:    "OK",
	}, nil
}
