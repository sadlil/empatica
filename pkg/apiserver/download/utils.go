package download

import (
	"strings"

	"github.com/bradfitz/latlong"
	"github.com/golang/glog"
	"gitlab.com/sadlil/empatica/apis/download"
	"gitlab.com/sadlil/empatica/pkg/location"
	"gitlab.com/sadlil/empatica/pkg/utils/random"
)

var locationService = location.NewLocationService()

const (
	MaxTry = 10
)

func randomDownload() *download.Download {
	for i := 1; i <= MaxTry; i++ {
		lat, long := random.LatLong()
		country, err := locationService.CountryFromLatLong(lat, long)
		if err != nil {
			glog.Errorln("Error getting Country name for Location Service", err)
			continue
		}
		glog.V(6).Infoln("Got country", *country)
		if len(country.CountryName) > 0 {
			return &download.Download{
				AppId:       random.String(4),
				Longitude:   long,
				Latitude:    lat,
				CountryName: country.CountryName,
				DownloadAt:  random.Time().Unix(),
			}
		}
	}

	// This not exactly country name. This is Timezone.
	// If we failed to get the country name. Lets try with
	// timezone.
	for i := 1; i <= MaxTry; i++ {
		lat, long := random.LatLong()
		name := latlong.LookupZoneName(lat, long)
		if len(name) > 0 {
			return &download.Download{
				AppId:       random.String(3),
				Longitude:   long,
				Latitude:    lat,
				CountryName: strings.Split(name, "/")[1],
				DownloadAt:  random.Time().Unix(),
			}
		}
	}
	return nil
}
