package random

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz")

func String(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func Time() time.Time {
	return time.Unix(rand.Int63n(time.Now().Unix()), 0)
}

func LatLong() (float64, float64) {
	long := rand.Float64() * 180
	if rand.Intn(100) <= 0 {
		long *= -1
	}
	lat := rand.Float64() * 90
	if rand.Intn(100) <= 0 {
		lat *= -1
	}
	return lat, long
}
