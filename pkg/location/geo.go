package location

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/golang/glog"
)

type Country struct {
	Languages   string `json:"languages"`
	CountryCode string `json:"countryCode"`
	CountryName string `json:"countryName"`
}

type LocationService struct {
	client *http.Client
}

func NewLocationService() *LocationService {
	return &LocationService{
		client: http.DefaultClient,
	}
}

func (l *LocationService) CountryFromLatLong(lat float64, long float64) (*Country, error) {
	glog.V(4).Infoln("Connecting location Service for country name")
	url := fmt.Sprintf(
		"http://ws.geonames.org/countryCodeJSON?lat=%.3f&lng=%.3f&username=sadlil",
		lat,
		long,
	)
	glog.V(4).Infoln("Calling Url", url)
	resp, err := l.client.Get(url)
	if err != nil {
		glog.Errorln("Location service encountered error", err)
		return nil, err
	}

	responseByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	ct := &Country{}
	err = json.Unmarshal(responseByte, ct)
	if err != nil {
		return nil, err
	}

	return ct, err
}
