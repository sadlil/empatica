package storage

import (
	"os"

	"path"

	"github.com/boltdb/bolt"
)

type Connection struct {
	Store *bolt.DB
}

func Init(db, _string string) error {
	if _, err := os.Stat(_string); err != nil {
		if err := os.MkdirAll(path.Dir(_string), 0777); err != nil {
			return err
		}
		if _, err := os.Create(_string); err != nil {
			return err
		}
	}
	store, err := bolt.Open(_string, 0600, nil)
	if err != nil {
		return err
	}

	if err := store.Update(func(tx *bolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists([]byte(DownloadBucketKey)); err != nil {
			return err
		}
		return nil
	}); err != nil {
		return nil
	}

	staticStorageProvider.store = store
	return nil
}

func Close() {
	staticStorageProvider.store.Close()
}

type storage struct {
	store *bolt.DB
}

var staticStorageProvider storage

func NewSession() *Connection {
	return &Connection{staticStorageProvider.store}
}
