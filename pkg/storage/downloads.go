package storage

import (
	"errors"

	"github.com/boltdb/bolt"
	"github.com/golang/glog"
	"github.com/golang/protobuf/proto"
	"gitlab.com/sadlil/empatica/apis/download"
	"gitlab.com/sadlil/empatica/pkg/notifications"
	"gitlab.com/sadlil/empatica/pkg/utils/random"
)

const (
	DownloadBucketKey = "downloads"
)

func (s *Connection) InsertDownloads(dw ...*download.Download) error {
	for _, data := range dw {
		if err := s.Store.Update(func(tx *bolt.Tx) error {
			glog.V(4).Infoln("Storing download", *data)
			b, err := tx.CreateBucketIfNotExists([]byte(DownloadBucketKey))
			if err != nil {
				return err
			}
			bytes, err := proto.Marshal(data)
			if err != nil {
				return err
			}
			b.Put([]byte(random.String(25)), bytes)
			notifications.Manager().Notify("download", `{"content": "new"}`)
			return nil
		}); err != nil {
			return err
		}
	}
	return nil
}

func (s *Connection) GetDownloads() ([]*download.Download, error) {
	dws := make([]*download.Download, 0)
	if err := s.Store.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(DownloadBucketKey))
		if b == nil {
			return errors.New("No Bucket Found")
		}
		b.ForEach(func(k, v []byte) error {
			dw := &download.Download{}
			if err := proto.Unmarshal(v, dw); err != nil {
				return err
			}
			dws = append(dws, dw)
			return nil
		})
		return nil
	}); err != nil {
		return nil, err
	}
	return dws, nil
}
