SHELL := /bin/bash

.PHONY: all
all: fmt vet install

.PHONY: clean
clean:
	rm -rf build
	rm -rf bin
	rm -rf assets/webapp
	rm -f ${GOPATH}/bin/empatica-server
	# make clean -C webapp

.PHONY: install
install: clean gen fmt
	go install ./cmd/... ./pkg/...

.PHONY: fmt
fmt:
	goimports -w cmd pkg
	gofmt -s -w cmd pkg

.PHONY: vet
vet:
	go vet ./pkg/... ./cmd/...

.PHONY: test
test:
	go test -v ./...

.PHONE: dep
dep:
	glide up -v
	glide vc --only-code --no-tests

.PHONY: gen
gen: web-build
	go-bindata -ignore=\\.go -mode=0644 -pkg=webapp -o=./assets/webapp/data.go ./webapp/src/static/... ./webapp/src/index.html ./webapp/src/dist/bundle.js ./webapp/src/app/templates/... ./webapp/src/app/styles/...

.PHONY: gen-proto
gen-proto:
	./hack/gen-apis.sh

.PHONY: web-des
web-deps:
	make dep -C webapp

.PHONY: web-build
web-build:
	make build -C webapp

.PHONY: go-build
go-build: install
	CGO_ENABLED=0 go build -o bin/empatica-server cmd/empatica-server/*go

.PHONY: docker-build
docker-build: go-build
	cp bin/empatica-server deploy/docker/empatica-server
	docker build -f=deploy/docker/Dockerfile -t sadlil/empatica:v0.1 .
	docker build -f=deploy/docker/Dockerfile -t sadlil/empatica .
	rm deploy/docker/empatica-server