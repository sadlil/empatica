#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

RETVAL=0
ROOT=$PWD

DIR="$(dirname "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )")/apis"
pushd ${DIR}

clean() {
    echo 'cleaning generated protobuf files'
	(find . | grep pb.go | xargs rm) || true
	(find . | grep pb.gw.go | xargs rm) || true
}

gen_proto() {
  if [ $(ls -1 *.proto 2>/dev/null | wc -l) = 0 ]; then
    return
  fi
  rm -rf *.pb.go
  protoc -I /usr/local/include -I . \
         -I ${GOPATH}/src/github.com \
         -I ${GOPATH}/src/github.com/googleapis/googleapis/ \
         --go_out=plugins=grpc:. *.proto
}

gen_gateway_proto() {
  if [ $(ls -1 *.proto 2>/dev/null | wc -l) = 0 ]; then
    return
  fi
  rm -rf *.pb.gw.go
  echo 'cleaning generated protobuf files'
  protoc -I /usr/local/include -I . \
         -I ${GOPATH}/src/github.com \
         -I ${GOPATH}/src/github.com/googleapis/googleapis/ \
         --grpc-gateway_out=logtostderr=true:. *.proto
}

gen_server_protos() {
	for d in */ ; do
      pushd ${d}
      gen_proto
      if dirs=$( ls -1 -F | grep "^v.*/" | tr -d "/" ); then
        for dd in $dirs ; do
          pushd ${dd}
          gen_proto
          popd
        done
      fi
      popd
    done
}

gen_proxy_protos() {
    for d in */ ; do
      pushd ${d}
      gen_gateway_proto
      if dirs=$( ls -1 -F | grep "^v.*/" | tr -d "/" ); then
        for dd in $dirs ; do
          pushd ${dd}
          gen_gateway_proto
          popd
        done
      fi
      popd
    done
}

compile() {
  go install ./...
}

gen_protos() {
  clean
  gen_server_protos
  gen_proxy_protos
  compile
}

if [ $# -eq 0 ]; then
	gen_protos
	popd
	exit $RETVAL
fi

case "$1" in
  compile)
      compile
      ;;
	server)
		gen_server_protos
		;;
	proxy)
		gen_proxy_protos
		;;
	clean)
	  clean
	  ;;
	*)  echo $"Usage: $0 {compile|server|proxy|clean}"
		RETVAL=1
		;;
esac

popd
exit $RETVAL