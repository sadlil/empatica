#!/usr/bin/env bash

#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

RETVAL=0
ROOT=$PWD


setup_protoc() {
    sudo apt-get -y install curl unzip git build-essential automake libtool
    sudo rm -rf /tmp/grpc
    sudo mkdir -p /tmp
    pushd /tmp
    git clone https://github.com/grpc/grpc
    cd grpc
    git submodule update --init
    cd /tmp/grpc/third_party/protobuf
    sudo ./autogen.sh && sudo ./configure && sudo make
    sudo make install
    sudo ldconfig
    cd /tmp/grpc
    sudo make
    sudo make install
    popd
}

setup_grpc_gateway() {
	rm -rf $GOPATH/src/github.com/googleapis/googleapis
	go get -u github.com/googleapis/googleapis || true
	go get -u golang.org/x/...
	rm -rf $GOPATH/src/google.golang.org/genproto
	go get google.golang.org/genproto || true
	rm -rf $GOPATH/src/google.golang.org/grpc
	go get -u google.golang.org/grpc
	pushd $GOPATH/src/google.golang.org/grpc
	git checkout v1.2.0
	popd
	rm -rf $GOPATH/src/github.com/golang/protobuf
	go get -u github.com/golang/protobuf/protoc-gen-go
	go get -u github.com/grpc-ecosystem/grpc-gateway
	pushd $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway
	go install ./protoc-gen-grpc-gateway/...
	go install ./protoc-gen-swagger/...
	popd
}

setup_go() {
    go get github.com/jteeuwen/go-bindata/...
}

setup_js() {
    npm install -g webpack webpack-dev-server
    npm install -g typings
    typings install dt~es6-promise dt~es6-collections --global --save
}

setup() {
	setup_protoc
	setup_grpc_gateway
	setup_go
	setup_js
}

if [ $# -eq 0 ]; then
	setup
	exit ${RETVAL}
fi

case "$1" in
	protoc)
		setup_protoc
		;;
	gateway)
		setup_grpc_gateway
		;;
	go)
		setup_go
		;;
	js)
		setup_js
		;;
	all)
		setup
		;;
	*)  echo $"Usage: $0 {protoc|gateway|go|js|all}"
		RETVAL=1
		;;
esac

exit ${RETVAL}