package assets

import (
	"os"

	"github.com/elazarl/go-bindata-assetfs"
	"gitlab.com/sadlil/empatica/assets/webapp"
)

func LoadWebAPP() *assetfs.AssetFS {
	return &assetfs.AssetFS{
		Asset:     webapp.Asset,
		AssetDir:  webapp.AssetDir,
		AssetInfo: os.Stat,
		Prefix:    "webapp/src",
	}
}
