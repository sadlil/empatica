package cmds

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/sadlil/empatica/pkg/version"
)

func NewVersionCmd() *cobra.Command {
	versionCmd := &cobra.Command{
		Use:   "version",
		Short: "Prints binary version number.",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Printf("%#v\n", version.Get())
		},
	}
	return versionCmd
}
