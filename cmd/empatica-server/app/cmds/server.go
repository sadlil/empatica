package cmds

import (
	"os"

	"github.com/golang/glog"
	"github.com/spf13/cobra"
	"gitlab.com/sadlil/empatica/pkg/servermachinary"
)

func NewServeCmd() *cobra.Command {
	config := servermachinary.ServerConfigs()
	cmd := &cobra.Command{
		Use:   "serve",
		Short: "Serve Webserver",
		Run: func(cmd *cobra.Command, args []string) {
			if err := config.Serve(); err != nil {
				glog.Errorln(err)
				os.Exit(1)
			}
		},
	}
	config.OverrideFromFlags(cmd.Flags())
	return cmd
}
