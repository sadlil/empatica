package app

import (
	"flag"

	"github.com/spf13/cobra"
	"gitlab.com/sadlil/empatica/cmd/empatica-server/app/cmds"
	"gitlab.com/sadlil/empatica/pkg/utils/logging"
)

func init() {
	logging.Init()
}

type App struct {
	cmds *cobra.Command
}

func New() *App {
	app := &App{
		cmds: &cobra.Command{
			Use:   "empatica-server",
			Short: "empatica-server commands",
			Run: func(cmd *cobra.Command, args []string) {
				cmd.Help()
			},
		},
	}
	app.AddChildCommands()
	return app
}

func (a *App) Run() error {
	defer logging.FlushLogs()
	if err := a.cmds.Execute(); err != nil {
		return err
	}
	return nil
}

func (app *App) AddChildCommands() {
	app.cmds.PersistentFlags().AddGoFlagSet(flag.CommandLine)
	app.cmds.AddCommand(cmds.NewServeCmd())
	app.cmds.AddCommand(cmds.NewVersionCmd())
}
