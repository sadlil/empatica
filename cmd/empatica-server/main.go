package main

import (
	"log"
	"os"

	"gitlab.com/sadlil/empatica/cmd/empatica-server/app"
)

func main() {
	if err := app.New().Run(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
	os.Exit(0)
}
